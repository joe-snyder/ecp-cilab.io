:orphan:

Runner Deployment
=================

.. _config.toml: runner-admin.html#configuration
.. _federation: runner-admin.html#id4
.. _federated: runner-admin.html#id4
.. _pre-clone-script: runner-admin.html#admin-defined-commands
.. _setuid: runner-admin.html#setuid

.. important::

   All further development efforts have been migrated to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_,
   support for the complete `fork <https://gitlbab.com/ecp-ci/gitlab-runner>`_
   has been discontinued. We are committed to continuing support
   for all ECP enhancements in this greatly improved CI model. All feedback
   is welcome as we continue to work towards a production ready release.

Source
------

Source code for runner associated efforts can be found in a fork of the
`GitLab-Runner <https://gitlab.com/ecp-ci/gitlab-runner>`_. However, codes
managing with the Batch executor are managed in a separate repository
(`golang-hpc <https://gitlab.com/ecp-ci/golang-hpc>`_).

See the `Building from Source`_ for the appropriate procedures.

Versioning
~~~~~~~~~~

The HPC focused enhancements found in the fork of the GitLab runner in
the ECP-CI repository are versioned separately from GitLab. However, due
to the critical nature of knowing the upstream version you will often
see versions / tags defined as such:

.. code-block:: console

    $ gitlab-runner --version
    Version:      12.4.0~hpc.0.5.2

Versioning of the RPM and release notes will still revolve around the
HPC modification versions (e.g. ``0.5.2``). Including the upstream
runner version is again just to provide additional transparency to the
users.

RPM
~~~

As of release ``12.6.0-hpc.0.5.3`` all generated RPMs can be found linked
to the associated `tagged release <https://gitlab.com/ecp-ci/gitlab-runner/-/tags>`_

Additionally, changes have been made to the RPM creation process that
differ from previous versions released as well as potentially
from `upstream GitLab <https://packages.gitlab.com/runner/gitlab-runner>`_.

  - There is no longer the creation of a ``gitlab-runner`` account
    automatically. It is up to system administrators to decide on
    the need for a runner specific service account.

  - All post installation scripts will no longer attempt to
    interact with Docker.

  - There are no longer scripts included (e.g. post-install), they
    are accounted for correctly in the spec file.

  - Upgrading will no longer remove any systemd files (e.g.
    ``/etc/systemd/system/gitlab-runner.service``). Please note this
    is only during upgrades. Uninstall and then installing a runner
    will still cause this to occur.

  - Excluding the above changes the aim was to keep in line with the
    traditional runner installation.

Requirements
------------

The standard GitLab Runner has a minimal set of
`requirements <https://docs.gitlab.com/runner/#requirements>`_.
However the enhancements made to the runner targeting HPC testing have
several new requirements that administrators must account for.

* Linux only
* `root` user permissions (see: setuid_)
* Git version 2.9+ (see: pre-clone-script_)
* Scheduler interactions are limited to:
    * Cobalt (tested versions: `1.0.28`)
    * LFS (tested versions: `???`)
    * Slurm (tested versions: `18.08.7`, `19.05.5`)

Compatibility Chart
~~~~~~~~~~~~~~~~~~~

See the official
`Comparability chart <https://docs.gitlab.com/runner/executors/#compatibility-chart>`_
for the complete list of supported features by different executors. We attempt
to preserve all original functionality and simply add to the following:

+------------+-------+-------+------+-------+------------+
| Executor   | Batch | Shell | SSH  | Docker| VirtualBox |
+============+=======+=======+======+=======+============+
| Setuid     | √     | √     | x    | x     | x          |
+------------+-------+-------+------+-------+------------+
| Federation | √     | √     | x    | x     | x          |
+------------+-------+-------+------+-------+------------+
| HPC        | √     | x     | x    | x     | x          |
| Scheduler  |       |       |      |       |            |
+------------+-------+-------+------+-------+------------+
| Token      | √     | √     | x    | x     | x          |
| handling   |       |       |      |       |            |
+------------+-------+-------+------+-------+------------+

All enhancements target HPC and have been focused on the Shell and Batch
executors **only**. We currently have no plans to extend support to the other
executor types. It is envisioned that the runners installed on HPC systems are
targeting the locally available resources, traditional Docker or Kubernetes
executors would reside on separate physically infrastructure. As such we
recommend use of the enhanced runner for batch / shell only.

Installation
------------

The process for installing the HPC focused runner differs from the
`official documentation <https://docs.gitlab.com/runner/install/linux-repository.html>`_
slightly. RPM are available from the
`forked GitLab repository tags <https://gitlab.com/ecp-ci/gitlab-runner/-/tags>`_.
However, if you are working with the provided RPM than the process
is a simple as:

1. ``$ sudo rpm -i gitlab-runner...`` or ``$ sudo rpm -Uvh gitlab-runner...``

After the installation is completed successfully we recommend reviewing the
`Registration`_ section.

Building from Source
--------------------

If you choose to instead build your own binary to support testing efforts,
you will be required to install:

* Git
* Make
* `Go <https://golang.org/dl/>`_ version 1.14.2+

Proceed with the following:

1. ``git clone https://gitlab.com/ecp-ci/gitlab-runner.git``
#. ``cd gitlab-runner``
#. ``make build_simple CGO_ENABLED=1 CC=gcc CXX=g++``

Once completed you can locate the generated binary
located under the ``out/binaries`` directory.

Registration
------------

Upon successfully installing the runner you will need to register it
with a valid GitLab server. For general information on the process please
see the official `Configuring GitLab Runners <https://docs.gitlab.com/ee/ci/runners/>`_
documentation.

The majority of the registration process has remained consistent
between this and the official runner, though there are several difference
in the process to allow for the setuid_ and federation_ enhancements to be
properly configured. These can be observed in both the Interactive_ as well
as Non-Interactive_ registration. Alternatively you can leverage
tools such as teh `GitLab Runner Auth`_ to assist.

GitLab Runner Auth
~~~~~~~~~~~~~~~~~~

In order to simplify the GitLab runner registration we recommend using
`LLNL/gitlab-runner-auth <https://github.com/LLNL/gitlab-runner-auth>`_
project. By leveraging this script as part of the ``ExecStartPre`` you
can more easily manage runners on stateless as well as stateful
resources.

Interactive
~~~~~~~~~~~

The easiest method by which to manually register an individual runner is through
the supported `interactive method <https://docs.gitlab.com/runner/register/#gnulinux>`_.
This is the default behavior when simply entering
``$ gitlab-runner register``. There are additional
`advanced configuration <https://docs.gitlab.com/runner/configuration/advanced-configuration.html>`_
options that can be be managed via the config.toml_ after successful
registration.

.. code-block:: console

    $ gitlab-runner register
    Runtime platform                                    arch=amd64 os=linux pid=1936 revision=126fb476 version=12.4.0~hpc.0.5.1
    Running in system-mode.

    Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
    https://gitlab.example.com
    Please enter the gitlab-ci token for this runner:
    token
    Please enter the gitlab-ci description for this runner:
    [hostname]: interactive-example
    Please enter the gitlab-ci tags for this runner (comma separated):
    interactive,example
    Is this a SetUID runner (true/false)?
    [false]: true
    Please specify a data directory for SetUID to work in:
    /ecp/runner
    Is this a federated runner (true/false)?
    [false]: false
    Registering runner... succeeded                     runner=BDWqPHxg
    Please enter the executor: docker-ssh, shell, ssh, batch, docker-ssh+machine, custom, docker, parallels, virtualbox, docker+machine, kubernetes:
    batch
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Non-Interactive
~~~~~~~~~~~~~~~

Another supported method is that of the non-interactive
registration. Prior to attempting a non-interactive registration it
is advised that you identify the list of configuration options with
``$ gitlab-runner register --help``.

.. code-block:: console

    $ gitlab-runner register --help
    Runtime platform                                    arch=amd64 os=linux pid=28459 revision=b502b01c version=12.4.0~hpc.0.5.1
    NAME:
        gitlab-runner register - register a new runner

    USAGE:
        gitlab-runner register [command options] [arguments...]

    OPTIONS:
        ...
        --setuid                                                     Set the UID of the executor to the logged-in user in the Gitlab Web UI. [$RUNNER_SETUID]
        --setuid-data-dir value                                      Directory where SetUID Runner builds and cache are stored. If set to $HOME, each will be stored in the given users home directory, otherwise each SetUID Runner will have it's own user-specific workspace below this directory in the form of /users/<user> [$RUNNER_SETUID_DATA_DIR]
        --setuid-user-whitelist value                                Used only if SetUID is enabled (true). An authoritative list of users who can execute Gitlab SetUID Runners. [$RUNNER_SETUID_USER_WHITELIST]
        --setuid-user-blacklist value                                Used only if SetUID is enabled (true). A list of usernames that are not allowed to run CI jobs. More authoritative than group whitelist / blacklist, but can be overridden by SetUIDUserWhitelist. [$RUNNER_SETUID_USER_BLACKLIST]
        --setuid-groups-whitelist value                              Use only if SetUID is enabled. A list of groups that are allowed to run CI jobs. Least authoritative [$RUNNER_SETUID_GROUP_WHITELIST]
        --setuid-groups-blacklist value                              Use only if SetUID is enabled. A list of groups that are not allowed to run CI jobs. [$RUNNER_SETUID_GROUP_BLACKLIST]
        --setuid-allow-run-as                                        When enabled will use a specified RunAs user for SetUID [$RUNNER_SETUID_ALLOW_RUN_AS]
        ...

Take note that each individual option has both a command line argument
(e.g. ``--setuid``) as well as potential environment variables
(e.g. ``$RUNNER_SETUID``) that can be used. In the case of non-interactive
registration it is possible to:

* Use arguments as seen in the
  `One-line registration command <https://docs.gitlab.com/runner/register/#one-line-registration-command>`_
  documentation.
* Set target environmental variables and that will be observed by the runner.
* Any combination of the two.

For example:

.. code-block:: bash

    #!/bin/bash

    export CI_SERVER_URL="https://gitlab.example.com"
    export RUNNER_SETUID_DATA_DIR='/ecp/runner'
    export RUNNER_EXECUTOR="batch"
    export RUNNER_SETUID=true
    export REGISTER_RUN_UNTAGGED=false
    export RUNNER_NAME=non-interactive-example
    export RUNNER_TAG_LIST=non-interactive-example

    /usr/bin/gitlab-runner register \
        --non-interactive

Federation Specific
~~~~~~~~~~~~~~~~~~~

In addition to the registration options added to support setuid_
it is required that if you wish to configure a federated_ runner
that is accomplished during the registration process. For example, if
we wish to register a federated runner interactively that will
rely on the ``oneid`` auth provided configured by the server
administrator and required a `1:1`
`assurance level <https://pages.nist.gov/800-63-3/sp800-63-3.html>`_:

.. code-block:: console

    $ gitlab-runner register
    ...
    Is this a federated runner (true/false)?
    [false]: true
    Enter an external auth source provider:
    oneid
    Enter the minimum accepted IAL (0-3), 0 to ignore:
    [0]: 1
    Enter the minimum accepted AAL (0-3), 0 to ignore:
    [0]: 1
    Optionally, enter a script that will be used to validate federated authentication (e.g. /etc/gitlab-runner/validate):
    /etc/gitlab-runner/oneid-validate

Take note that configuration options regarding federation (with the exception
of the ``validate-auth`` script) cannot be altered once established. This is
true even though the options appear in the config.toml_. This is a design
decisions as we view federation as an agreement between runner and server.
And changes on either end should constitute a new registration process.

.. warning::

    At this time there is no validation for the administrator defined
    `external auth source provider`. Please confirm
    with the server administrator to ensure a valid auth source has been
    identified before registering.

For more completed information on how federation_ is accomplished in
the runner as well as details on all potential configuration options,
see the associated documentation.

Runner Tokens
~~~~~~~~~~~~~

As part of the runner registration process you will interact with two
distinct but equally sensitive tokens. Depending on the
`type of runner <https://docs.gitlab.com/ee/ci/runners/#shared-specific-and-group-runners>`_
you will be required to obtain
the associated **registration token**. This token is only
used during the registration process; however, the same token can be utilized
until it has been reset from the server. Resetting
the registration token will **not** revoke access of any runner registered
with that token.

Finally, after a successful registration the runner will be provided with
a new **authentication token**. It is specifically scoped
to the registered runner and used to authenticate all runner
interactions with the GitLab server. It can be seen in plain text
in the ``config.toml``:

.. code-block:: toml

    [[runners]]
        token = "<authentication token>"

.. warning::

    The server recognizes runners by their unique tokens; however, a token
    is only tied to a specific runner/configuration by existing in the
    associated ``config.toml``. There are no limitations in place to prevent
    a token from being used incorrectly to spoof a runner if illicitly obtained.

The token's existence in the ``config.toml`` is required and
as such should be protected to the best of your ability. Only the
runner user is required to have read access to this file, as such it
is advised to confirm correct file permissions have been set.

System Service
--------------

The runner supports several `system services <https://docs.gitlab.com/runner/configuration/init.html#overriding-systemd>`_
though for the purposes of this documentation we are focusing on
`systemd <https://github.com/systemd/systemd/>`_ exclusively.

.. note::

    The default `GitLab runner RPM <https://packages.gitlab.com/runner/gitlab-runner>`_
    has defined behavior to reinstall the service leading to the potential
    recreation of the unit file
    (``/etc/systemd/system/gitlab-runner.service``). As this
    behavior can be undesirable, the `ECP-CI fork <https://gitlab.com/ecp-ci/gitlab-runner/>`_
    we will no longer support automatically managing services
    during RPM installation/uninstalling from `13.0.0` onwards.

Managing the creation of the service file can be down manually by an
administrator using the ``gitlab-runner uninstall`` and
``gitlab-runner install`` commands respectively. For instance, if we wish to
install a new service:

.. code-block:: console

    $ gitlab-runner install --user root --service example
    Runtime platform        arch=amd64 os=linux pid=3632 revision=4b30b4d1 version=12.4.0~hpc.0.5.2

    $ cat /etc/systemd/system/example.service
    [Unit]
    Description=GitLab Runner
    ...

    $ gitlab-runner uninstall --service example
    Runtime platform        arch=amd64 os=linux pid=3674 revision=4b30b4d1 version=12.4.0~hpc.0.5.2

Now if we examine an example service file it is possible to highlight several
recommended settings that may be worth considering for you deployment:

.. code-block::

    [Unit]
    Description=GitLab Runner
    After=syslog.target network.target
    ConditionFileIsExecutable=/usr/lib/gitlab-runner/gitlab-runner

    [Service]
    StartLimitInterval=5
    StartLimitBurst=10
    ExecStartPre=/path/to/gitlab_runner_auth.py --api-url https://gitlab.example.com --prefix /path/to/runner/prefix --stateless
    ExecStart=/path/to/gitlab-runner run --config /path/to/runner/prefix/config.toml --working-directory /example/gitlab-runner --config --service gitlab-runner --syslog --user root
    Restart=always
    RestartSec=120
    StandardOutput=null
    StandardError=null

    [Install]
    WantedBy=multi-user.target


The ``ExecStartPre`` is set to take advantage of `GitLab Runner Auth`_ if the
functionality benefits your systems configuration / deployment strategy. By
specifically ``--syslog`` in the run command it will correctly integrate
with the system logging service. As such to avoid duplication of output is
recommend to defined both ``StandardOutput`` as well as ``StandardError`` to
*null*. Finally, the ``ExecStart`` has been configured to use the appropriate
available `runner parameters <https://docs.gitlab.com/runner/commands/#gitlab-runner-run>`_

It is possible to
`override systemd <https://docs.gitlab.com/runner/configuration/init.html#overriding-systemd>`_
kill signal behavior for the runner. Please see the official documentation
to define expected behaviors.

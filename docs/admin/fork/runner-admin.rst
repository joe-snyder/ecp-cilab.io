:orphan:

Runner Administration
=====================

.. _advanced configuration: https://docs.gitlab.com/runner/configuration/advanced-configuration.html
.. _batch executor: runner-batch.html
.. _federation: runner-federation.html
.. _requirements: runner-deploy.html#requirements

.. important::

   All further development efforts have been migrated to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_,
   support for the complete `fork <https://gitlbab.com/ecp-ci/gitlab-runner>`_
   has been discontinued. We are committed to continuing support
   for all ECP enhancements in this greatly improved CI model. All feedback
   is welcome as we continue to work towards a production ready release.

Configuration
-------------

The GitLab runner configuration is managed via a
`TOML <https://github.com/toml-lang/toml>`_ file. If you have not used TOML
previously and have questions please refer to the project's
`README <https://github.com/toml-lang/toml/blob/master/README.md>`_ as it
provides excellent documentation. Additionally, GitLab has organized
documentation covering a number of topis relating to
`configuring a GitLab runner <https://docs.gitlab.com/runner/configuration/>`_.
The documentation here will only focus on aspects of the configuration that
directly relate to Setuid_, `batch executor`_, and other aspects focused on
HPC administration.

.. code-block:: toml

    # this is a simple example config.toml file
    # global section
    concurrent = 5

    # runner specific section
    [[runners]]
        name = "shell-setuid-example"
        url = "https://gitlab.example.com"
        token = "<TOKEN>"
        pre_clone_script = '''
            ml use /example/modules/Core && ml git
        '''
        output_limit = 10000
        setuid = true
        setuid_data_dir = "/ecp/gitlab-runner"
        setuid_user_whitelist = [
            "alice",
            "david",
        ]
        setuid_user_blacklist = ["paul"]
        setuid_group_whitelist = ["ecp"]

Managing the TOML File
~~~~~~~~~~~~~~~~~~~~~~

By default the configuration can be found under the ``/etc/gitlab-runner``
directory. You can specify a different configuration file when launching
the runner, for example:

``$ gitlab-runner run --config /fs/gitlab-configuration/config.toml``

In this case the runner will use the configuration file
specified via the ``--config`` flag as opposed to the default location. This
may prove beneficial if for instance you have the runner deployed on a
stateless node and want to manage the configuration from a mounted directory.

Changes to the file can be made at any time, the runner will detect any
alterations and reload the configuration. This will be observed by all
subsequent jobs. The action is also logged by the runner:

.. code-block::

    Configuration loaded                                builds=0

Finally, it is **strongly advised** the the ``config.toml`` file is owned and
accessed excursively by the owner as the runner token should
be protected.

Concurrent Jobs
~~~~~~~~~~~~~~~

.. code-block:: toml

    concurrent = 5

A single ``config.toml`` can have numerous runners registered and managed.
Each appears under a separate ``[[runners]]`` table. Regardless of the number
of registered runners there is a upper limited defined by ``concurrent`` on
the number of total jobs that can be running at any given time.

In our above example the limit is set to `5` however as an admin this can
be altered to best fit the limitations of your Ci environment.

.. note::

    At time there is no recommendation for a concurrent number established
    for HPC workloads. It may require experimentation but keep in mind that the
    runner is **not** a scheduler. As such it will not take into account the
    availability of system resources when accepting new jobs.

Beneficial GitLab Settings
~~~~~~~~~~~~~~~~~~~~~~~~~~

By default GitLab provides runner administrators with an array of potential
`advanced configuration <https://docs.gitlab.com/runner/configuration/advanced-configuration.html>`_
options. We recommend exploring upstream documentation for a complete
list. There are several specific options that we have found to provided
value in our deployments or have specific interactions with the
enhanced runner.

.. csv-table::
    :header: "Setting", "Type", "Description"
    :widths: 20, 15, 80

    "``pre_clone_script``", "string", "`Commands <runner-admin.html#admin-defined-commands>`_ to be executed on the Runner before cloning the Git repository. this can be used to adjust the Git client configuration first, for example. To insert multiple commands, use a (triple-quoted) multi-line string or “\n” character. The ``setuid`` setting is observed."
    "``pre_build_script``", "string", "`Commands <runner-admin.html#admin-defined-commands>`_ to be executed on the Runner after cloning the Git repository, but before executing the build. To insert multiple commands, use a (triple-quoted) multi-line string or “\n” character. The ``setuid`` setting is observed."
    "``post_build_script``", "string", "`Commands <runner-admin.html#admin-defined-commands>`_ to be executed on the Runner just after executing the build, but before executing after_script. To insert multiple commands, use a (triple-quoted) multi-line string or “\n” character. The ``setuid`` setting is observed."
    "``output_limit``", "integer", "`Defines <runner-admin.html#output-limit>`_ the maximum build log size (in kilobytes)"

:sub:`Description source: https://docs.gitlab.com/runner/configuration/advanced-configuration.html`

Admin Defined Commands
""""""""""""""""""""""

.. code-block:: toml

    [[runners]]
        pre_clone_script = '''
            ml use /example/modules/Core && ml git
        '''

The ``pre_clone_script`` is outlined in the
`GitLab's advanced configuration documentation. <https://docs.gitlab.com/runner/configuration/advanced-configuration.html>`_
However, it can be used to inject administrator defined commands into a user's
CI job at predefined points. In the above example we are leveraging
`LMOD <https://lmod.readthedocs.io/en/latest/>`_ to ensure the required
version of Git is available before required by the CI job.

.. note::

    As specified in the Requirements_ Git version 2.9+ needs to be available
    in order to use the enhanced runner. However, this newer version of Git is
    only technically required during the `get_sources` phase of a job. By
    leveraging the above method you can avoid installing a newer system wide
    version of Git.

The changes to the environnement (``module use`` pre-append to the
``MODULEPATH``) will only be present during this `get_sources` phase of the
job. Each subsequent phase of the jobs, including when the user defined
scripts are executed, will occur in a clean environment.

In addition to the ``pre_clone_script`` there also exists options
``pre_build_script`` and ``post_build_script`` that work the same way.
With all three of these options it is important to note that the
subsequent commands will observe the Setuid_ configuration.

Output Limit
""""""""""""

.. code-block:: toml

    [[runners]]
        output_limit = 10000

The maximum build log size (in kilobytes) is defined on the runner
level. Though this functionality exists in the
`upstream GitLab runner <https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section>`_
teams with larger build/test process have been likely to
experience issues with default settings.

If a user's  output from a CI job exceeds the default limit (4MB)
the job will fail and they will see the following error message:
``Job's log exceeded limit of 4194304 bytes``.

Setuid
~~~~~~

.. csv-table::
    :header: "Setting", "Type", "Description"
    :widths: 20, 15, 80

    "``setuid``", "boolean", "To enable setuid functionality set `true`, default value is `false`."
    "``setuid_data_dir``", "string", "Absolute path for job's base working directory, runner will dictate the remaining folder structures and ensure proper ownership."
    "``setuid_user_blacklist``", "string array", "List of local usernames that are prohibited from executing CI jobs."
    "``setuid_user_whitelist``", "string array", "List of local usernames that are allowed to execute CI jobs."
    "``setuid_group_blacklist``", "string array", "List of local groups that are prohibited from executing CI jobs."
    "``setuid_group_whitelist``", "string array", "List of local groups that are allowed to execute CI jobs."

Setuid functionality enables execution of CI jobs on bare-metal
even on multi-tenant systems in a secure manner. Traditionally the runner
would execute an entire CI job as a single user. With setuid
enabled it will instead be executed by the GitLab user's
local account. For detailed documentation please see:

.. toctree::
   :maxdepth: 3

   setuid.rst

.. warning::

    At this time ``setuid`` is only supported for the Shell and Batch
    executors. It will not be observed with other executors.

The goal of the setuid extension is to ensure that, given a shared computing
environment, users are provided the ability to run CI jobs on these resources
under their associated local account. Providing access not only to desired
computer resources but also ensuring that these CI jobs will observe local
access restrictions placed upon the user.

This is accomplished by requiring a relationship between user accounts on the
GitLab front-end and accounts on the underlying systems where the runner
is hosted. The CI job forks a process under the controlling user's system
account, which ensures that commands run during the CI job will fail if they
impact files or systems that the user does not have access to.

.. note::

    Using the runner with setuid enabled carries with it the requirement
    that the runner be run by ``root`` user. At this time there is no support
    for setuid via capabilities or any other available mechanisms.

User Shell
""""""""""

Default shell behavior can be observed in GitLab's
`supported shells <https://docs.gitlab.com/runner/shells/#shbash-shells>`_
documentation. Though by enabling ``setuid`` the behavior has been
updated slightly in favor of improved security the fundamental
concept remains the same. As such you can expect to see all jobs
executed in a clean login shell owned by the associated user. All
environmental variables from the runner are cleansed before any
user scripts can be executed.

Authorization Flow
------------------

From an administration perspective, the enhanced runner is meant to provide an
increased level of accountability to CI jobs that would otherwise execute
under the service account responsible for the runner. The target of the
setuid effort was to establish a comprehensive process in the runner that
would ultimately result in the subsequent CI job being executed by a local
user.

.. note::

    Ignoring the goals the federation efforts there is a fundamental
    assumption underlying all setuid enhancements. The GitLab server
    accounts must be managed using the same underlying systems as those
    found on the target CI system. Meaning *userA* on GitLab is the
    same as *userA* on the system.

To assist in the setuid process there several optional steps in the
runner's internal workflow that as an admin you can exert control over.

.. figure:: files/setuid_runner_workflow.png

* Federation: Mechanism by which server managed authentication providers
  and they associated payload can be further validated at a runner level.
* Blacklist & Whitelist: Control over the users/groups that are allowed to
  execute jobs using a defined runner. See
  `Blacklist and Whitelist <setuid.html#blacklist-and-whitelist>`_
  for complete details.
* Run As (Service) User: Mapping a local username username to the appropriate
  service account.

The culmination of this flow is a job executing under not just a valid
local username but also one that local runner administrators have complete
control over.

Federation
----------

The goal of federation within the `Authorization Flow`_ is fundamentally
to translate a GitLab server user to their valid local account by
validating with an authentication token provided by a facility
managed IdP to the GitLab server. This token is only meaningful to the
IdP.

.. note::

    All federation enhancements are currently under active development.
    Future changes to the runner can occur but care will be given to avoid
    breaking changes to any workflows/scripts defined in accordance
    with this documentation.

Once you've `registered <runner-deploy.html#federation-specific>`_
your federated runner you will be expected to provide a script that
will be used for validation.

Validate Auth
~~~~~~~~~~~~~

.. code-block:: toml

    [[runners]]
        federated = true
        validate_auth = "/etc/gitlab-runner/auth.script"

The ``validate_auth`` allows for any script/application to be defined.
When ``federated`` is enabled. This will be automatically invoked and provided
the following context:

1. The authorization token from the IdP for that user will be made available
   to that script via the environment variable ``GITLAB_AUTH_TOKEN``.
2. The first command line argument will be the federated user name. This is
   a username provided in the payload from the IdP to the server. It is viewed
   as the accepted username. Depending on the IdP This information may not
   be provided and if it is not established by the end of the workflow
   the job will fail.

As the GitLab Runner user (usually ``root``) the script will
be initiated through Go's `exec package <https://golang.org/pkg/os/exec/>`_.
Any administrator desired actions or additional validation can occur so
long as the results conform to the following chart.

.. image:: files/validate_fed_flow.png
        :scale: 60%

It is also possible ot override the federated username with a valid
JSON payload returned with standard output from your script. Invalid
JSON will constitute a job failure; however, mistakes in valid
formatting will not be checked. Special care should be given to ensure
that output provided is correct.

.. code-block:: javascript

    {
        username: "<string>"
    }

Do not create any *stdout* if you do not wish to override any values as
in worst case this can be confused with invalid JSON which will cause
failed jobs.

Run As User
-----------

.. warning::

    At this time where exists a fundamental flaw in the RunAs user
    process. To understand this it is important to note that each and
    every CI job is provided with a job specific personal access token
    that is scoped to the user who triggered the job. This means that
    even after a user may be validated to run under a target service
    account that token is still scoped to their GitLab account.

    .. image:: files/serviceuser-jobtoken.png
        :scale: 25%

    It is *important* to understand the implications of this before
    enabling the functionality. Efforts are underway provide a
    meaningful fix but at this time the issue remains open. If working
    in an environment where this could be exploited to access
    potentially sensitive repositories it is recommended to not use
    service user account and simply rely on traditional setuid.

.. code-block:: toml

    [[runners]]
        setuid = true
        setuid_allow_run_as = true
        setuid_run_as_user = "TARGET_RUNAS_USER"
        setuid_validate_run_as = "/etc/gitlab-runner/test.sh"

The Run As user configuration (also referred to as service user) targets
supporting the a requirement at site that all job be executed under a
service account as opposed to an individuals account. Controlling the
process of if a service account can be used is left to the
runner administrator manage with the `Validate Run As`_ script.

Run As User Key
~~~~~~~~~~~~~~~

The ``setuid_run_as_user`` defines the name of the variable the RunAs user
can be provided as part of the CI process. This aims to allows CI user/teams
to define a target user but does not provide a guarantee to the
user the requested target is acceptable (see `Validate Run As`_)

For example, in the above we've defined
``setuid_run_as_user = "TARGET_RUNAS_USER"``. Thusly during the job's
setuid preparations we will look for that key in order to potentially
provide the user's defined value. The
`priority of environment variables <https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables>`_
is observed during this process.

This configuration is optional both for you the administrator as well
as the CI user. The existence of an associated value will directly
influence the `Validate Run As`_ script execution.

Validate Run As
~~~~~~~~~~~~~~~

The ``setuid_validate_run_as`` provides administrators with a mechanism by
which they are able control the service user validation process through a
separately defined application. It is **required** in order to use the
`Run As User`_ functionality, failing to define it will lead to the following
error message appearing in CI job:

.. code-block:: console

    RunAs system error
    No SetUIDValidateRunAs script defined: please contact you administrator

The script is invoked as part of the setuid validation process,
in one of two ways:

1. ``$ script gitLabUser`` - If no `Run As User Key`_ can be identified then
   the script will be invoked with only

2. ``$ script runAsUser gitLabUser`` - The user has provided a target user by
   the `Run As User Key`_.

A simple example of this script may appear as such:

.. code-block:: bash

    #!/bin/bash
    if [ "$#" -eq 1 ]; then
        user="$1"
        if [ "$user" == "gitlabUser" ] ; then
            echo '{"username": "newUser"}'
        exit 0
    fi
    elif [ "$#" -eq 2 ]; then
        user="$2"
        service_account="$1"
    fi
    if [ "$service_account" == "passTest" ] ; then
        exit 0
    fi
    exit 1

Though in our example we've chosen to use Bash any executable can work as
long as the potential positional arguments are dealt with accordingly. The
script/application defined only needs to be accessible by the GitLab runner
user. All calls will be made prior to the completion of the setuid
preparations.

It is also possible ot override the target username with a valid
JSON payload returned with standard output from your script. Invalid
JSON will constitute a job failure; however, mistakes in valid
formatting will not be checked. Special care should be given to ensure
that output provided is correct.

.. code-block:: javascript

    {
        username: "<string>"
    }

The complete flow for this process is:

.. image:: files/validate_runas_flow.png
        :scale: 60%

Job Token Handling
------------------

For each CI job a unique
`job token <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#job-token>`_
is generated. This is scoped to the user account's read access to projects and
remains valid only for the duration of that specific job. In the traditional
model the runner would have generated scripts that would use this token via the
command line. In multi-tenant environments however, users can inspect
all commands of running processes.

The enhanced runner takes steps to hide all tokens from appearing as command
line arguments in any runner generated commands. This includes
uploading/downloading artifacts, and all Git interactions by carefully
leveraging the
`GIT_ASKPASS env variable. <https://git-scm.com/docs/gitcredentials>`_

Administrators may wish to note that there are options to enforce permissions
on `/proc` in the form of
`mount options <https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/tree/Documentation/filesystems/proc.txt#n2014>`_
however, we assume this is not configured and thusly steps are taken by
default to hide potential sensitive commands.

Temporary Files/Directories
---------------------------

There are two major cases where the upstream GitLab runner (as well
as any ECP manage fork) will create temporary files and directories:

1. As of `release 12.0 <https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1315>`_
   all job traces will be stored to disk.
2. When using the custom executor a temporary directory is used
   to transfer job scripts to the assigned driver.

In both cases the folder permissions are set so only the runner
user will have access. In many cases this will be ``root`` or
some other privileged service user.

.. code-block:: console

    [10:50:30] user@ci-test:/tmp
    $ ll | grep trace
    drwx------ 1 root    root     94B Jun 17 10:49 custom-executor026800128
    -rw------- 1 root    root     14K Jun 17 10:50 trace036573548
    -rw------- 1 root    root     14K Jun 17 10:50 trace184916677

.. note::

    A runner that has crashed or was improperly shutdown will not
    automatically cleanup any of these temporary files/directories.
    Else the cleanup will be handled without any administrator
    intervention.

The temporary directory used for storing files is determined
by Go's  `os.TempDir() <https://golang.org/pkg/os/#TempDir>`_ function. In
the case of Unix systems the default value will be ``/tmp`` unless there is
a value set for ``$TMPDIR``.

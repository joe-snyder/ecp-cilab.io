Jacamar-Auth
============

.. _config: configuration.html
.. |br| raw:: html

    <br>

Authorization Flow
------------------

For administrators, Jacamar-Auth is meant to provide an increased level
of accountability to CI jobs that would otherwise all execute under
a single responsible service account. To support this goal a comprehensive
yet flexible approach has been implemented that tightly couples;
CI job context, local configurations, and administrator defined
policies. The culmination of this authorization workflow is a trusted local
account who can be targeted for a downscoping process and
ultimately executor the CI job.

.. image:: files/authorization_jacamar_workflow.png

.. important::

    An **important assumption** is that server accounts are managed using the
    same underlying systems as those found on the target CI system. Meaning
    *userA* on GitLab is the same as *userA* on the system. Of equal importance
    is that they are unable to influence the potential username on the GitLab
    server. For additional details see the
    `Security Considerations <../server-admin.html#security-considerations>`_
    in the server documentation.

Core to this entire workflow is the *job context* provided that allows
for us to establish not only definitive information regarding the job
itself but the GitLab user who triggered it. This is accomplished
by leveraging several key trusted CI variables provided to the custom
executor along with the CI
`job JWT <https://gitlab.com/gitlab-org/gitlab/-/issues/207125>`_.
Together, along with Jacamar's config_ file, we are able to
establish the verifiable context for the entire job.

There several optional steps in the
runner's internal workflow that as an admin you can exert control over.

* `RunAs User`_: Mapping a local username username to the appropriate
  local account. Often times this local account is a service user; however,
  the context provided to the script can be used to identify any valid
  username under which the job will run.
* `Allow/Block Lists`_: Control over the users/groups that are allowed to
  execute jobs based upon memberships.
* `Downscoping Mechanisms`_: Leverages authorized user context to drop
  all current permissions and run the associated CI job as that user.
  After downscoping has occurred Jacamar-Auth will only continue running
  for the purposes of piping stdout, stderr, and exit status
  back the runner process.

RunAs User
----------

.. warning::

    At this time there exists a fundamental flaw in the RunAs user
    process when targeting a shared service account.
    To understand this it is important to note that each and
    every CI job is provided with a job specific personal access token
    that is scoped to the user who triggered the job. This means that
    even after a user may be validated to run under a target service
    account that token is still scoped to their GitLab account.

    .. image:: files/serviceuser-jobtoken.png

    It is *important* to understand the implications of this before
    enabling the functionality. Efforts are underway provide a
    meaningful fix but at this time the issue remains open. If working
    in an environment where this could be exploited to access
    potentially sensitive repositories it is recommended to not use
    service user account and simply rely on traditional setuid.

.. code-block:: toml

    [auth.runas]
      validation_script = "/custom/run-validate.py"
      user_variable = "TARGET_SERVICE_USER"
      sha256 = "e258d248fda94c63753607f7c4494ee0fcbe92f1a76bfdac795c9d84101eb317"

The RunAs user configuration (also referred to as service account) targets
supporting a requirement at site that all job be executed under a
service account as opposed to an individual's account. Controlling the
process of which service account a CI trigger user can be access is left to
the runner administrator manage with the Validation_ script.

RunAs User Variable
~~~~~~~~~~~~~~~~~~~

The ``user_variable`` defines the name of the variable the ``RunAs`` user
can be provided as part of the CI process. This aims to allows CI user/teams
to define a target user but does not provide a guarantee to the
user the requested target is acceptable (see Validation_)

For example, in the above we've defined
``user_variable = "TARGET_SERVICE_USER"``. Thus during the job's
setuid preparations we will look for that key in order to potentially
provide the user's defined value. The
`priority of environment variables <https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables>`_
is observed during this process.

.. important::

  The value of this variable should not be trusted, instead it should
  be viewed only as a request that can then be validated using the
  defined script.

This configuration is optional both for you the administrator as well
as the CI user. The existence of an associated value will directly
influence the Validation_ script execution.

Validation
~~~~~~~~~~

The ``validation_script`` provides administrators with a mechanism by
which they are able control the service user validation process through a
separately defined application. It is **required** in order to use the
`RunAs User`_ functionality.

The script is invoked as part of the authorization flow
in one of two ways:

1. ``$ script username`` - If no `RunAs User Variable`_ can be identified
   then the script will be invoked only providing the currently identified
   trigger user's local account.

2. ``$ script verifier username`` - The user has provided a target user by
   the `RunAs User Variable`_ and will be provided as the ``verifier``.

Note that the ``username`` provided is the currently assumed
local username of the associated account.
It is **important** that in cases when a username
can be influenced by the user that you leverage other provided
environment variables to validate the requester.

.. hidden-code-block:: bash
    :starthidden: True
    :label: Simple test example Bash validation_script.

        #!/bin/bash
        if [ "$#" -eq 1 ]; then
            user="$1"
            if [ "$user" == "gitlabUser" ] ; then
                echo '{"username": "newUser"}'
            exit 0
        fi
        elif [ "$#" -eq 2 ]; then
            user="$2"
            service_account="$1"
        fi
        if [ "$service_account" == "passTest" ] ; then
            exit 0
        fi
        exit 1

|br|

Though in our example we've chosen to use Bash any executable can work as
long as the potential positional arguments are dealt with accordingly. The
script/application defined only needs to be accessible by the GitLab runner
user.

In addition to command line arguments, as of
`Jacamar 0.3.0 <../../releasenotes/jacamar/jacamar_0.3.0.html>`_
additional context derived from the
`GitLab Job JWT <https://gitlab.com/gitlab-org/gitlab/-/issues/207125>`_
is provided to the script via  environment variables:

.. list-table::
    :widths: 10 50
    :header-rows: 1

    * - Key
      - Description
    * - ``JWT_USER_LOGIN``
      - Login username of the user who started the job. Can be user changed depending on server deployment (**always verify** before trusting this value).
    * - ``JWT_USER_EMAIL``
      - Primary email of the user who started the job.
    * - ``JWT_USER_ID``
      - Identification number of the user who started the job.
    * - ``JWT_NAMESPACE_ID``
      - Unique ID given to a username or group name that the current project belongs to.
    * - ``JWT_PROJECT_ID``
      - Unique ID of the current project.
    * - ``JWT_PROJECT_PATH``
      - Human readable namespace for the project.
    * - ``JWT_JOB_ID``
      - Unique ID of the current job that GitLab uses internally.
    * - ``JWT_PIPELINE_ID``
      - Unique ID of the current CI pipeline.
    * - ``RUNAS_TARGET_USER``
      - User proposed account that can be the target for downscoping if approved (same as command line argument ``verifier``).
    * - ``RUNAS_CURRENT_USER``
      - Currently identified local user account of the CI trigger user. This **can differ** from the JWT's UserLogin depending on configuration of the authorization flow (same as command line argument ``username``).

It is also possible to override the target username with a valid
JSON payload returned with standard output from your script. Invalid
JSON will constitute a job failure; however, mistakes in valid
formatting will not be checked. Special care should be given to ensure
that output provided is correct.

.. code-block:: javascript

  {
      username: "<string>",
      gitlab_account: "<string>",
      data_dir: "<string>"
  }

The complete flow for this process is:

RunAs Workflow
~~~~~~~~~~~~~~

.. image:: files/validate_runas_flow.png

Allow/Block Lists
-----------------

The user/group lists provide direct control over who has access to the runner.
At several points during authorization the currently identified target user's
local account will
be evaluated against these rules. If a failure state is encountered
the job will end before any user influenced code can be executed. An error
message for this failure will be logged; however, the user facing information
is generalized to avoid exposing specifics of the configuration.

- ``user_allowlist`` - An authoritative list of users who can execute
  Gitlab SetUID Runners.
- ``user_blocklist`` - A list of usernames that are not allowed to run
  CI jobs. More authoritative than group allowlist / blocklist, but can be
  overridden by user allowlist.
- ``groups_blocklist`` - A list of groups that are not allowed to run
  CI jobs.
- ``groups_allowlist`` - A list of groups that are allowed to run CI
  jobs. Least authoritative.


All lists can be configured within Jacamar's
`Auth table <configuration.html#auth-table>`_ by defining
an array of local user/group names.

.. code-block:: toml

  [auth]
    user_allowlist = ["usr1"]
    user_blocklist = ["usr2", "usr3"]
    groups_allowlist = ["grp1", "grp2"]
    groups_blocklist = ["grp3"]

Any usernames or groups provided are assumed to relate to Linux user/groups
on the local system. The runner **does not** ensure the lists configured in
are in fact valid and mistakes can allow undesirable job results.


Shell Allowlist
~~~~~~~~~~~~~~~

In addition to a series of user/group allowlists we offer a
``shell_allowlist``. Similarly to other lists the user database is consulted
to identify information regarding regarding the local target CI user. If
defined, this provides an authoritative list of acceptable shells. Any
deviations from this list will result in a job failure.

.. code-block:: toml

  [auth]
    shell_allowlist = ["/bin/bash", "/bin/zsh"]

In our above example, only user's who default shell has been defined as either
``/bin/bash`` or ``/bin/zsh`` will be allowed to execute jobs. The purpose of
this functionality is to support tests environments that may choose to
leverage such configuration to temporary restrict access
(e.g. ``/usr/sbin/nologin``).

.. note::

  Regardless of the default shell of the user all jobs will be executed via
  a clean Bash shell.

Downscoping Mechanisms
----------------------

Finally after the `Authorization Flow`_ has successfully completed we are
able to act upon the validated user by downscoping permissions to
this target and beginning the CI execution process completely as them.

.. code-block:: toml

  [auth]
    downscope = "setuid"

.. list-table::
    :widths: 10 50
    :header-rows: 1

    * - Value
      - Description
    * - ``setuid``
      - Levering underlying system calls to *setuid*/*setgid* when creating a child process for actual job execution.
    * - ``sudo``
      - Construct a targeted command (``sudo -E su <username> -m -s /bin/bash --pty -c jacamar <stage>``), relying on the *su* application to enforce downscoping of permissions.
    * - ``none``
      - Indicates you wish to leverage the authorization capabilities of ``jacamar-auth`` but the job will be run **without** downscoping permissions.
    * - *Default*
      - There is no *default* value to ``downscope`` and not specifying one when using ``jacamar-auth`` will result in a failed job.

For our core mechanism, ``setuid``, we are relying on Go's
`implementation <https://github.com/golang/go/blob/master/src/syscall/exec_linux.go>`_
of setuid for spawning a child process owned by the user via
`credential package <https://golang.org/pkg/syscall/?GOOS=linux#Credential>`_.
This child process will be tightly controlled and a user
owned shell that will launch ``Jacamar`` with all the necessary
context to execute the CI job in the administrator expected
manner. Only *stdout*/*stderr* will be piped back to the runner,
thus preserving the custom executor model while still realizing
desired `Security Model`_.

.. note::

  If you are familiar with previous iterations of the ECP runner
  enhancements then you might have used ``setuid = true``. Though
  this has changed to ``downscope = "setuid"`` we currently only
  support the setuid related operation. There are plans to
  expand this in the future.

Security Model
~~~~~~~~~~~~~~

The authorization functionality (identifying and authorizing a local user
to execute the CI job) relies on the
`CI job permissions model <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html>`_
that was introduced in GitLab 8.12. In particular, the following points
from the CI job permission spec are key to understand:

1. Job permissions should be tightly integrated with the permissions of a
   user who is triggering a job;
#. It is simple and convenient that your job can access everything that
   you as a user have access to;
#. We (in this case, runner host machines) already know what the user is
   allowed to do;
#. Any job that is triggered by the user, is also marked by GitLab with
   their permissions;
#. GitLab already fully knows who is triggering a job.

The downscoping feature extends these principles onto the runner's host
system by executing CI jobs as their local account.
Since CI jobs now run as unprivileged user processes, job permissions
carry over to the OS, and they can access files and
directories in the host filesystem that their user has access to.
The server is still ultimately responsible for determining a
job's permissions, but instead of simply issuing the job tokens,
the server also tells the Jacamar who to run as.

Job Token Handling
------------------

For each CI job a unique
`job token <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#job-token>`_
is generated. This is scoped to user's account, provides read access
to all their projects, and remains valid for the duration of the job.
In the traditional model the runner generate job scripts that would
use this token via the command line. In multi-tenant environments however,
users can `potentially <https://man7.org/linux/man-pages/man5/proc.5.html>`_
inspect all commands of running processes.

Jacamar takes steps to hide all tokens from appearing as command
line arguments in any runner generated commands. This includes
uploading/downloading artifacts, and all Git interactions by carefully
leveraging the `GIT_ASKPASS env variable. <https://git-scm.com/docs/gitcredentials>`_

Administrators may wish to note that there are options to enforce permissions
on ``/proc`` in the form of
`mount options <https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git/tree/Documentation/filesystems/proc.txt#n2014>`_
however, we assume this is not configured.

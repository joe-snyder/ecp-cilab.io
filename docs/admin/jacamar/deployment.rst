Deployment
==========

Due the design of Jacamar CI and the requirement to adhere to the
structure of `custom executor <https://docs.gitlab.com/runner/executors/custom.html>`_
the deployment process consists of more than a single application.
It is a multiple step process encompassing:

* `Installation`_: Either from supported packages_ or from source the process
  of installing not just Jacamar CI but also a patched version of the
  GitLab-Runner.
* `Runner Registration`_: Registering the runner with the GitLab server
  and accounting for custom executor requirements and advanced
  management.
* `Custom Executor Configuration`_: Specific steps required in order to
  configure the executor to correctly interact with Jacamar CI.
* `System Service`_: Establishing a systemd service for the runner process.

By engaging feedback and better understanding the desired deployment methods
we are currently working to streamline the process.
Additional comments are always welcome.

Installation
------------

.. important::

    There are several key enhancements we currently
    `patch <https://gitlab.com/ecp-ci/jacamar-ci/-/tree/develop/build/package/rpm/patches>`_
    into the runner as part of the build. Attempting to use an
    official upstream release will lead to failed jobs.

Requirements
~~~~~~~~~~~~

Jacamar's runtime requirements:

* Bash
* Git versions 2.9+
* GitLab Runner with patches
  (`additional requirements <https://docs.gitlab.com/runner/#requirements>`_)
* libc

In addition, if you wish to configure support for any
`scheduler <executors.html>`_ then you are currently
limited too:

* Cobalt
* LFS
* Slurm

Packages
~~~~~~~~

Jacamar CI provides several ways to obtain supported packages for
installation:

* As of version `0.3.0 <../../releasenotes/jacamar/jacamar_0.3.0.html>`_,
  you can find packages for every
  `release <https://gitlab.com/ecp-ci/jacamar-ci/-/releases>`_.
* To assist those in testing prior to release you can find all compiled
  binaries and RPMs created during any CI pipeline in the
  *build-results* job.

We **highly** recommend using the
`official release <https://gitlab.com/ecp-ci/jacamar-ci/-/releases>`_
unless you are attempting to test specific functionality.

In addition to options available in obtaining the packages, there
is currently the added Requirements_ of a patched GitLab Runner. This
has lead to two distinct packages being supplied with every release:

1. Patched GitLab-Runner with key ECP enhancements
   needed the use of Jacamar
   (e.g. ``gitlab-runner-ecp-13.5.0-1.el7.x86_64.rpm``). It has been
   differentiated from the traditional runner and should
   conflict if attempted to be deployed on the same system.
2. Jacamar CI RPM (e.g. ``jacamar-ci-0.3.0-1.el7.x86_64.rpm``)
   is the core package and relies directly upon the
   ``gitlab-runner-ecp``.

.. Important::

    As of Jacamar
    `version 0.4.2 <../../releasenotes/jacamar/jacamar_0.4.2.html>`_
    the potentially privileged ``jacamar-auth`` application
    will be deployed to a restricted ``/opt/jacamar/bin``
    directory.

Build from Source
~~~~~~~~~~~~~~~~~

Build requirements:

* Bash
* Git
* `Go <https://golang.org/dl/>`_ versions 1.15+
* Make
* libc

There are also containers available in Jacamar's
`container registry <https://gitlab.com/ecp-ci/jacamar-ci/container_registry>`_
that can be used for builds as all requirements are already
accounted for.

.. code-block:: console

    $ rpm -qlp jacamar-ci-*.rpm
    /opt/jacamar
    /opt/jacamar/bin
    /opt/jacamar/bin/jacamar-auth
    /usr/bin/jacamar

    $ rpm -qip jacamar-ci-*.rpm
    Name        : jacamar-ci
    Version     : 0.4.2
    ...

Jacamar
^^^^^^^

.. code-block:: console

  git clone https://gitlab.com/ecp-ci/jacamar-ci.git
  cd jacamar-ci
  make build
  make install PREFIX=/usr/local

Runner (Spack)
^^^^^^^^^^^^^^

To ease this process of building a patched versions of the runner
we've created a local `Spack <https://spack.io/>`_ repository:

.. code-block:: console

  spack repo add $(pwd)/build/package/spack/jacamar-repo
  spack install gitlab-runner+jacamar

Runner Registration
-------------------

For general information on the process please
see the `Configuring GitLab Runners <https://docs.gitlab.com/ee/ci/runners/>`_
documentation. We advise also referring to the official documentation for
complete details on both the
`interactive <https://docs.gitlab.com/runner/register/#gnulinux>`_
as well as
`command line <https://docs.gitlab.com/runner/register/#one-line-registration-command>`_
registration methods. Keeping in mind there are additional potential
`advanced configuration <https://docs.gitlab.com/runner/configuration/advanced-configuration.html>`_
options that can be be managed via the ``config.toml`` after successful
registration.

.. important::

  You will need to select the ``custom`` executor type in order
  to use Jacamar CI. However, you can change this setting at any time
  so if you originally registered using a different executor
  type that is okay.

GitLab Runner Auth
~~~~~~~~~~~~~~~~~~

In order to simplify the GitLab runner registration we recommend using
`LLNL/gitlab-runner-auth <https://github.com/LLNL/gitlab-runner-auth>`_
project. By leveraging this script as part of the ``ExecStartPre`` you
can more easily manage runners on stateless as well as stateful
resources.

Managing the TOML File
~~~~~~~~~~~~~~~~~~~~~~

By default the configuration for the GitLab Runner
can be found under the ``/etc/gitlab-runner``
directory. You can specify a different configuration file when launching
the runner, for example:

.. code-block:: console

  gitlab-runner run --config /fs/gitlab-configuration/config.toml

In this case the runner will use the configuration file
specified via the ``--config`` flag as opposed to the default location. This
may prove beneficial if for instance you have the runner deployed on a
stateless node and want to manage the configuration from a mounted directory.

Changes to the file can be made at any time, the runner will detect any
alterations and reload the configuration. This will be observed by all
subsequent jobs. The action is also logged by the runner:

.. code-block::

  Configuration loaded                                builds=0

Finally, it is **strongly advised** the the ``config.toml`` file is owned and
accessed excursively by the owner as the runner token should
be protected.

Runner Tokens
~~~~~~~~~~~~~

As part of the runner registration process you will interact with two
distinct but equally sensitive tokens. Depending on the
`type of runner <https://docs.gitlab.com/ee/ci/runners/#shared-specific-and-group-runners>`_
you will be required to obtain
the associated **registration token**. This token is only
used during the registration process; however, the same token can be utilized
until it has been reset from the server. Resetting
the registration token will **not** revoke access of any runner registered
with that token.

Finally, after a successful registration the runner will be provided with
a new **authentication token**. It is specifically scoped
to the registered runner and used to authenticate all runner
interactions with the GitLab server. It can be seen in plain text
in the ``config.toml``:

.. code-block:: toml

  [[runners]]
      token = "<authentication token>"

.. warning::

  The server recognizes runners by their unique tokens; however, a token
  is only tied to a specific runner/configuration by existing in the
  associated ``config.toml``. There are no limitations in place to prevent
  a token from being used incorrectly to spoof a runner if illicitly obtained.

The token's existence in the ``config.toml`` is required and
as such should be protected to the best of your ability. Only the
runner user is required to have read access to this file, as such it
is advised to confirm correct file permissions have been set.

Custom Executor Configuration
-----------------------------

Additional steps are required for the
`configuration of the custom executor <https://docs.gitlab.com/runner/executors/custom.html#configuration>`_.
Simply add the following to the table (``[runners.custom]``) to your
config using ``vim /etc/gitlab-runner/config.toml``.

.. code-block:: toml

  [runners.custom]
    config_exec = "jacamar-auth"
    config_args = ["config", "--configuration", "/etc/gitlab-runner/custom-config.toml"]
    prepare_exec = "jacamar-auth"
    prepare_args = ["prepare"]
    run_exec = "jacamar-auth"
    run_args = ["run"]
    cleanup_exec = "jacamar-auth"
    cleanup_args = ["cleanup", "--configuration", "/etc/gitlab-runner/custom-config.toml"]

By including the above configuration you fulfil requirements both of
the custom executor and Jacamar CI:

* Each stage within a CI job (e.g. ``config``, ``prepare``, ``run``, or
  ``cleanup``) must have an associated executable/script defined. In
  our case we are using ``jacamar-auth`` application which will provide
  the ability to take advantage of the
  `authorization and downscoping enhancements <auth.html>`_
  a downscoping mechanism (e.g. *setuid*).
* Additional arguments can be provided to ``jacamar-auth``, in this case
  it requires a sub-command related to the current stage.
* Jacamar requires its own set of configurations. We include them by
  specifying the ``--configuration`` argument and the location of the
  file we will be creating next.

For complete details on the configuration process please
see the `documentation <configuration.html>`_.

System Service
--------------

The runner supports several
`system services <https://docs.gitlab.com/runner/configuration/init.html#overriding-systemd>`_
though for the purposes of this documentation we are focusing on
`systemd <https://github.com/systemd/systemd/>`_ exclusively.

.. note::

  Runner management benefits from the configuration of the system service.
  Jacamar is designed to only be invoked by the runner so is not directly
  involved in these steps.

Managing the creation of the service file can be done manually by an
administrator using the ``gitlab-runner install`` and
``gitlab-runner uninstall`` commands respectively.

.. code-block:: console

  $ gitlab-runner install --user root --service example
  Runtime platform        arch=amd64 os=linux pid=3632 revision=4b30b4d1 version=13.4.0

  $ cat /etc/systemd/system/example.service
  [Unit]
  Description=GitLab Runner
  ...

  $ gitlab-runner uninstall --service example
  Runtime platform        arch=amd64 os=linux pid=3674 revision=4b30b4d1 version=13.4.0

Now if we examine an example service file it is possible to highlight several
recommended settings that may be worth considering for you deployment:

.. code-block::

  [Unit]
  Description=GitLab Runner
  After=syslog.target network.target
  ConditionFileIsExecutable=/usr/lib/gitlab-runner/gitlab-runner

  [Service]
  StartLimitInterval=5
  StartLimitBurst=10
  ExecStartPre=/path/to/gitlab_runner_auth.py --api-url https://gitlab.example.com --prefix /path/to/runner/prefix --stateless
  ExecStart=/path/to/gitlab-runner run --config /path/to/runner/prefix/config.toml --working-directory /example/gitlab-runner --config --service gitlab-runner --syslog --user root
  Restart=always
  RestartSec=120
  StandardOutput=null
  StandardError=null

  [Install]
  WantedBy=multi-user.target


* ``ExecStartPre`` is set to take advantage of `GitLab Runner Auth`_ if the
  functionality benefits your systems configuration / deployment strategy.
* The ``ExecStart`` has been configured to use the appropriate
  available `runner parameters <https://docs.gitlab.com/runner/commands/#gitlab-runner-run>`_
* ``gitlab-runner ... --syslog`` will correctly integrate
  with the system logging service.
* To avoid duplication of output is recommend to defined both
  ``StandardOutput`` as well as ``StandardError`` to *null*.

It is possible to
`override systemd <https://docs.gitlab.com/runner/configuration/init.html#overriding-systemd>`_
kill signal behavior for the runner. Please see the official documentation
to define expected behaviors.

Temporary Files/Directories
~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two major cases where the upstream GitLab runner (as well
as any ECP manage fork) will create temporary files and directories:

1. As of
   `release 12.0 <https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1315>`_
   all job traces will be stored to disk.
2. When using the custom executor a temporary directory is used
   to transfer job scripts to the assigned driver.

In both cases the folder permissions are set so only the runner
user will have access. In many cases this will be ``root`` or
some other privileged service user.

.. code-block:: console

  $ cd /tmp
  $ ll | grep trace
  drwx------ 1 root    root     94B Jun 17 10:49 custom-executor026800128
  -rw------- 1 root    root     14K Jun 17 10:50 trace036573548
  -rw------- 1 root    root     14K Jun 17 10:50 trace184916677

.. note::

  A runner that has crashed or was improperly shutdown will not
  automatically cleanup any of these temporary files/directories.
  Else the cleanup will be handled without any administrator
  intervention.

The temporary directory used for storing files is determined
by Go's  `os.TempDir() <https://golang.org/pkg/os/#TempDir>`_ function. In
the case of Unix systems the default value will be ``/tmp`` unless there is
a value set for ``$TMPDIR``, this can be defined in the service file.

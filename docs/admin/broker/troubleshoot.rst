Troubleshooting
===============

.. important::

    The `CI Token Broker <https://gitlab.com/ecp-ci/ci-token-broker>`_
    is under active development and subject to change.
    We **strongly** advise that you only deploy in a testing
    capacity until the official 1.0 release.

The token broker is a proposed tool that address several unique
concerns to our supported CI models. As such we've chosen to highlight
potential issues an administrator may experience here
along with the recommended solution.

User not allowed to access a project
------------------------------------

During a CI job the user attempts to access resources on a project and are
unexpectedly met with a "You are not allow to download code from this
project" message:

.. code-block:: console

    $ whoami
    service_user_a
    $ git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@broker.example.com/group/project.git
    Cloning into 'project'...
    remote: You are not allowed to download code from this project.
    fatal: unable to access 'https://broker.example.com/group/project.git/': The requested URL returned error: 403

Due to the structure of the broker access to any project is
limited to that of the service account **not** the ``CI_JOB_TOKEN``
itself. To address this please make sure that the service
account (in this example ``service_user_a``) user is granted `Guest`
`project permissions <https://docs.gitlab.com/ee/user/project/members/>`_.

Jacamar CI v0.7.1
=================

* *Release*: `v0.7.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.7.1>`_
* *Date*: 06/10/2021

Admin Changes
-------------

* Logging enhancements and correctly dial local syslog
  (`!188 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/188>`_,
  `!178 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/178>`_).

  - The previous iteration on logging did not properly establish a
    `connection to a log daemon <https://golang.org/pkg/log/syslog/#Dial>`_.
    This led to functional logging during initial testing but issues on more
    traditional test systems.

  - .. code-block:: toml

      [auth.logging]
        enabled = true
        location = "syslog"
        level = "debug"
        network = "tcp"
        address = "localhost:1234"

  - Optionally ``location`` can specify a static file.

* Support overriding data directory within RunAs workflow
  (`!187 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/187>`_).

  -  .. code-block:: javascript

      {
          username: "username",
          data_dir: "/target/directory/.ci"
      }

Bug & Development Fixes
-----------------------

* Correct RPMs builds on RHEL 8/CentOS 8
  (`!186 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/186>`_).
* Upgrade to Go version to *1.16.5*
  (`!189 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/189>`_).
* Target ``arm64`` if platform ``aarch64`` in Spack runner build
  (`!190 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/190>`_).
* Log JWT verification step
  (`!174 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/174>`_).
* Gather and log all RunAs validation script output
  (`!191 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/191>`_).

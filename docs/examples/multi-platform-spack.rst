Multiple Platform Extends with Spack
====================================

This example will provide a basic demonstration of how you can run
across multiple platforms/architectures while sharing common sections
of code. We will be extensively using both the
`extends <https://docs.gitlab.com/ee/ci/yaml/#extends>`_
as well as
`needs <https://docs.gitlab.com/ee/ci/yaml/#needs>`_
GitLab CI functionality. We are choosing to use
`Spack <https://spack.io/>`_ in our example; however, this can be
replaced with your project's specific build processes.

.. tabs::

    .. tab:: Generic

        .. literalinclude:: ../../source/multi-platform-spack/.gitlab-ci.generic.yml
            :language: yaml
            :linenos:

    .. tab:: ALCF

        .. note::

            Due to the limit on available test runners this example relies
            upon treating the shell executor (e.g. login node) as a different
            platform than the batch executor (e.g. compute environment).

        .. literalinclude:: ../../source/multi-platform-spack/.gitlab-ci.alcf.yml
            :language: yaml
            :linenos:

Source
------

Additional files that will be required in your project in order for the
example to function as intended.

deploy_spack.bash
~~~~~~~~~~~~~~~~~

This is a simple script that will fetch the `SPACK_COMMIT` Git sha into the
`SPACK_ROOT` directory specified via environment variables.

    .. literalinclude:: ../../source/multi-platform-spack/deploy_spack.bash
            :language: yaml
            :linenos:

spack.yaml
~~~~~~~~~~

With the use of Spack we are leveraging
`environments <https://spack.readthedocs.io/en/latest/environments.html>`_
extensively and as such
each potential location for example will require it's own file.

.. tabs::

    .. tab:: Generic

        .. literalinclude:: ../../source/multi-platform-spack/env/generic.spack.yaml
            :language: yaml
            :linenos:

    .. tab:: ALCF

        .. literalinclude:: ../../source/multi-platform-spack/env/alcf.spack.yaml
            :language: yaml
            :linenos:

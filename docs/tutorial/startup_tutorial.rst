ECP CI Startup Tutorial
=======================

.. _`batch executor`: https://ecp-ci.gitlab.io/docs/ci-users/ci-batch.html
.. _executors: https://docs.gitlab.com/runner/executors/
.. _gcovr: https://gcovr.com/en/stable/
.. _`pipeline editor`: https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#pipeline-editor
.. _runners: https://docs.gitlab.com/runner/
.. _`shell executor`: https://docs.gitlab.com/runner/executors/shell.html

.. yaml file links
.. _after_script: https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script
.. _before_script: https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script
.. _coverage: https://docs.gitlab.com/ee/ci/yaml/#coverage
.. _extends: https://docs.gitlab.com/ee/ci/yaml/#extends
.. _image: https://docs.gitlab.com/ee/ci/yaml/#image
.. _include: https://docs.gitlab.com/ee/ci/yaml/#include
.. _rules: https://docs.gitlab.com/ee/ci/yaml/#rules
.. _stage: https://docs.gitlab.com/ee/ci/yaml/#stages
.. _tags: https://docs.gitlab.com/ee/ci/yaml/#tags

.. list-table::
    :widths: 10 50
    :header-rows: 0

    * - Target Audience
      - HPC code teams that wish to begin using GitLab CI as part of their testing strategy.
    * - Requirements
      - Hands-on sections of the tutorial have been written for a select number of `Supported Instances`_, and requires a `compatible browser <https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers>`_.
    * - Estimated Time
      - 1.5 hours

Welcome to the CI Startup Tutorial! We want this tutorial to
provide guidance to HPC project teams interested in getting started, not
just with ECP resources but GitLab CI in general. We've written this
to offer as much flexibility as possible and provided hands-on
portions targeting a range of potential CI instances. Throughout
the document we will reference upstream documentation to further
clarify key functionality and would like to encourage you to make
use of the `official user docs <https://docs.gitlab.com/ee/user/>`_. The
official documentation offers comprehensive details and up-to-date
details.

Though we hope you are joining us for a conference tutorial, it
is by no means required as this hands-on can be entirely self-guided.

Supported Instances
-------------------

Though we've tried to normalize the example project amongst the target site
test resources that is not possible in all cases (e.g., facility specific
runners and configuration present different requirements). Wherever an option
is provided during this presentation, please always reference the appropriate
tab.

.. tabs::

    .. tab:: GitLab.com

      All hands-on portions of the tutorial can be completed using
      `gitlab.com <https://gitlab.com>`_ which offers a set number of CI/CD
      pipelines minutes to all users. If you do not have a account and wish
      to create one simply navigate to the `signup page <https://gitlab.com/users/sign_up>`_
      and follow the instructions:

      .. image:: files/startup/gitlab_com_signup.png
         :scale: 30%

    .. tab:: ALCF

      All hands-on portions of the tutorial can be completely using
      `gitlab-ci <https://gitlab-ci.alcf.anl.gov>`_ which offers CI/CD pipelines
      to authorized users. If you do not have an account and wish to create one,
      you can get more information at `account-information <https://www.alcf.anl.gov/support-center/account-and-project-management/account-information>`_

      .. image:: files/startup/gitlab-ci_alcf_sing_in.png
         :scale: 30%

    .. tab:: NERSC

      All hands-on portions of the tutorial can be completed using NERSC site-local
      gitlab `resources <https://software.nersc.gov>`_ which provides CI/CD capabilities
      to existing NERSC users. If you are not a current user, you can check out our documentation
      on how to become a NERSC user of an `existing project <https://docs.nersc.gov/accounts/#obtaining-a-user-account>`_
      or apply for a `new project <https://www.nersc.gov/users/accounts/allocations/first-allocation/>`_

      .. image:: files/startup/gitlab-ci_nersc_sign_in.png
         :scale: 30%

    .. tab:: OLCF

      For OLCF site local test resources...

      **More information coming soon!**

In all cases we **attempt to limit** the time requirements of
CI/CD jobs within the tutorial. We hope these constraints help to limit any
resources usages associated with your project/account to a reasonable
level.

Navigating GitLab CI
--------------------

Start by determining the *version* of your GitLab by selecting the `?` icon
from the top right corner and then `Help`. The GitLab version will be
displayed at the top of this page along with links to version/deployment
specific documentation.

.. Note::

    Some sites may choose to disable this option, in these cases please
    reference your site's local documentation to identify the instance
    variable.

.. image:: files/gitlab_help.png
      :scale: 40%

The *version* of the server plays an important role in the CI/CD process as
a majority of the upstream functionality you may wish to leverage is
directly influenced by this. For the purposes of all hands-on portions of
this tutorial we are expecting version *13.8+* and all `Supported Instances`_
meet this requirement.

Next you will want to
`fork the tutorial project <https://docs.gitlab.com/ee/user/project/working_with_projects.html#fork-a-project>`_
into your user namespace. Identify the source project based upon
the resources you will be using for the tutorial:

* `[GitLab.com] Startup Tutorial Project <https://gitlab.com/ecp-ci/tutorials/startup-tutorial>`_
* **More supported instances coming soon!**

.. note::

   We are excited to be using a mirror of the
   `Hello Numerical World <https://github.com/markcmiller86/hello-numerical-world>`_
   project. The application associated with this project solves a one
   dimensional heat conduction problem. For complete details, including
   the associated
   `Hands-On Lesson <https://xsdk-project.github.io/MathPackagesTraining2020/lessons/hand_coded_heat/>`_,
   please see the project's ``README.md``.

Upon successfully forking the project you will be automatically redirected:

.. image:: files/startup/successful_project_fork.png

Changes can be made freely to this newly created project copy as they can
only be suggested back to the source project through a
`merge request <https://docs.gitlab.com/ee/user/project/merge_requests/>`_.

Begin by navigating to `Settings` --> `CI/CD`. Please note that depending
on your screen size, you may need to collapse the sidebar.

.. image:: files/startup/gitlab_ci_navigation.png
      :scale: 30%

Once in the `CI/CD Settings` page you will notice a number of configuration
options. We encourage you to explore these if time permits, but there are
several that are important to highlight. In the `General Pipelines` section,
some key parameters are:

* **Timeout**: This is the default timeout (one hour) for each CI *job*. A
  CI *job*, however, is something quite different from the jobs run
  by a scheduler (e.g., wallclock) on behalf of a batch executor. It is
  important not to confuse these two wholly different timeout parameters.
  This value can also be overridden at the job level; however, neither case
  can exceed the runner's maximum timeout.
* **Custom CI Configuration Path**: This is the default path within the
  repository at which to find the YAML file controlling CI/CD behavior.
  The default is ``.gitlab-ci.yml``. This can useful to alter when
  managing CI across multiple mirrors.

.. image:: files/gitlab_ci_timeout.png
      :scale: 60%

If we `Collapse` the `General` section and `Expand` the `Runners` section,
we can view available *runners* (and register new ones). This page shows the
runner's current status (hopefully a green dot indicating it is healthy).
There is also a list of tags_ associated with the runner. Tags are defined
by whomever administers the runner and are used in ``.gitlab-ci.yml`` files
to *target* CI/CD *jobs* to specific runners. Knowing how to find these
tags is important.

.. important::

    In any future work with CI, regardless of platform, it is important
    to notes that it is your responsibility to observe all rules appropriate
    for the type of code/data you are managing. Please consult with your
    project teams and facility where required.

Introductory CI Workflow
------------------------

All CI/CD functionality we demonstrate in this tutorial is controlled via
a special `YAML <https://en.wikipedia.org/wiki/YAML>`_
file in the root directory of the repository named ``.gitlab-ci.yml``.
For this tutorial we recommend leveraging the `pipeline editor`_
which was released in server version `13.8`. This is a helpful feature
available regardless of the server's license level and offers helpful
functionality specifically for developing your project's CI/CD.

.. image:: files/startup/pipeline_editor.png

First, each job should be assigned a stage_. The naming of the
stages is arbitrary. In our example, we wish to have just a single stage
with the name ``make``.

.. code-block:: yaml

   stages:
     - make

`Variables <https://docs.gitlab.com/ee/ci/variables/README.html>`_
are an important concept in GitLab CI as each job will be
provided with a build environment that incorporates a number of
`predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
along with those developers may define. Let's define a
`global <https://docs.gitlab.com/ee/ci/yaml/README.html#variables>`_
variable(s) that will apply to all jobs within our pipeline.

Depending on where you are choosing to run your job you may be required
to supply additional site specific details via your job's CI variables.

.. tabs::

    .. tab:: GitLab.com

      .. code-block:: yaml

         stages:
           - make

         variables:
           MAKE_CXXFLAGS: "--coverage"

    .. tab:: ALCF

      .. code-block:: yaml

         stages:
           - make

         variables:
           MAKE_CXXFLAGS: "--coverage"
           ANL_THETA_PROJECT_SERVICE_USER: "<service account>"

    .. tab:: NERSC

      NERSC only provides a batch runner, which submits jobs to its Slurm scheduler.
      Consequently, CI jobs must provide scheduler parameters for the jobs to run.

      .. code-block:: yaml

         stages:
           - make

         variables:
           MAKE_CXXFLAGS: "--coverage"
           SCHEDULER_PARAMETERS: "-C haswell -q debug -N1 -t 00:05:00"



We can define the `job <https://docs.gitlab.com/ee/ci/jobs/>`_
itself, this can be an arbitrary name so long as it does not conflict
keys or the list of
`unavailable job names <https://docs.gitlab.com/ee/ci/yaml/#unavailable-names-for-jobs>`_.
In our case lets start by creating
a job entitled ``gcc-build`` and associating it with our ``make`` stage_:

.. code-block:: yaml

   stages:
     - make

   # ...

   gcc-build:
     stage: make

If you refer back to the `Navigating GitLab CI`_ you will recall that runners
are identified (*targeted*) via tags_. Any single matching tag could be
sufficient to uniquely identify it, so long as multiple runners do not share
that same tag. When multiple runners are available the server will attempt
to match the first available runner to a job with a subset of tags.

.. tabs::

    .. tab:: GitLab.com

      Since we are using the public GitLab instance, the runners available to us
      are `Docker executors <https://docs.gitlab.com/runner/executors/docker.html>`_,
      this means all jobs will execute in container based upon the defined image_:

      .. code-block:: yaml

       gcc-build:
         stage: make
         tags: [gitlab-org]
         image: registry.gitlab.com/ecp-ci/ecp-ci.gitlab.io/startup-tutorial/gcc:latest

      .. note::

        In our case we are pulling an image from the
        `GitLab container registry <https://docs.gitlab.com/ee/user/packages/container_registry/>`_;
        however, you are free to use other options such as Docker Hub.

    .. tab:: ALCF

      For ALCF Gitlab, the runners available are custom executors running on Theta.
      In this tutorial we are only using Shell custom executors.


      .. code-block:: yaml

       gcc-build:
         stage: make
         tags: [ecp,shell]

      .. note::

          We have two types of executor at ALCF, the Shell and Batch executors.

        - Shell executors are good for build processes, like compile code, create artifacts and such.

        - Batch executors run CI jobs through the HPC scheduler.


    .. tab:: NERSC

      The aforementioned batch runner can be leveraged by using its tag ``cori``.
      This runner is available instance-wide on our site-local gitlab instance.

      .. code-block:: yaml

       gcc-build:
         stage: make
         tags: [cori]

      .. note::

        All jobs will run in the users namespace with all the associated file permissions.


Add a ``before_script`` scoped to our job as well as the required
``script`` section. This will be the core of CI job, the arbitrarily defined
code will be executed on our required test resource. The default behaviors of
the runner will ensure the Git commit associated with you job is properly
fetched and made available prior to any user defined script running. The code
defined in each of these sections will be combined and used to a generate a
single `Bash <https://man7.org/linux/man-pages/man1/bash.1.html>`_
script that will be executed within the scope of the CI job.

.. code-block:: yaml

   gcc-build:
     # ...
     before_script:
       - hostname
       - env | grep -E 'GITLAB_|CI_'
     script:
       - make CXXFLAGS=${MAKE_CXXFLAGS} LDFLAGS="--coverage -lm" check

.. note::

   At ths time all runner generated scripts will executor using Bash_
   (potentially `sh <https://man7.org/linux/man-pages/man1/sh.1p.html>`_
   in very limited cases) regardless of your user's default
   configurations. Be cognoscente of these when constructing your
   job's ``script``.

Now that we have our core job script, when the pipeline is execute
we will not only build/test our ``heat`` application but prior
see a full list of `predefined variables`_.

.. tabs::

    .. tab:: GitLab.com

      .. code-block:: yaml

        gcc-build:
          # ...
          after_script:
            - pip3 install gcovr
            - gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR}

    .. tab:: ALCF

       .. code-block:: yaml

        gcc-build:
          # ...
          after_script:
            - python3 -m venv .venv
            - source ${CI_PROJECT_DIR}/.venv/bin/activate
            - pip3 install gcovr
            - gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR}

    .. tab:: NERSC

       .. code-block:: yaml

        gcc-build:
          # ...
          after_script:
            - python3 -m venv .venv
            - source ${CI_PROJECT_DIR}/.venv/bin/activate
            - pip3 install gcovr
            - gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR}

The ``artifacts`` keyword is used to specify a list of files
and/or directories that should be captured after a job has been completed.
These are then uploaded to the GitLab server, where they can be downloaded
manually and will be made available to jobs in subsequent stages of the
pipeline. In this example we are capturing a binary (``heat``) that we have
just compiled.

.. code-block:: yaml

      gcc-build:
        # ...
        artifacts:
          expire_in: 3 days
          paths:
            - heat
           reports:
             cobertura: coverage.xml

.. note::

    Using `artifacts <https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html>`_
    may not suite a number HPC project requirements as their our limits as to
    maximum size one can upload that are established by the server
    administrators. For more complete details and other options please see
    the `Artifacts, Caching, and Local Storage for HPC Project <https://ecp-ci.gitlab.io/docs/guides/hpc-artifacts-caching.html>`_
    guide.

In our example we've specified the
`Coberture <https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportscobertura>`_
report type as it offers a mechanisms for identifying code coverage
in merge requests:

.. image:: files/startup/cover_coverage_compare.png
    :scale: 35%

There are a range of potential
`reporting tools <https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreports>`_
that could benefit your teams workflows, and are worth investigating. Be aware
though that many such tools require a licensed version of the server.

Now that you've completed your first ``.gitlab-ci.yml`` file
*Commit changes* you have made in the editor. Once completed,
navigate to *CI/CD* -> *Pipelines*.

.. image:: files/startup/running_pipeline.png
    :scale: 30%

Troubleshooting your YAML
~~~~~~~~~~~~~~~~~~~~~~~~~

It is not uncommon to see the following error message when examining the
Pipelines page:

.. image:: files/startup/pipelines_yaml_error.png
    :scale: 30%

Hovering your mouse over the ``yaml invalid`` tag can provide additional
context regarding the error. Though, a more beneficial troubleshooting method
involves leveraging GitLab's builtin CI linter. The linter can be found within
the `pipeline editor`_:

.. image:: files/startup/pipelines_missing_stage.png
    :scale: 20%

In our above example the error encountered is a missing
``stages`` parameter, leading to the following error:

.. code-block::

   gcc-build job: chosen stage does not exist; available stages are .pre, build, test, deploy, .post

Reintroducing the code will correct this error. You may also notice that
in the ``before_script`` there is a typo with ``hostn`` as opposed to
``hostname``. The linter is only designed to identify mistakes in the
GitLab YAML syntax and any others error, even if they will lead to failed
jobs, will **not** be identified.

Completed - Intro CI
~~~~~~~~~~~~~~~~~~~~

Reference of the completed `Introductory CI Workflow`_ ``.gitlab-ci.yml`` file.

.. tabs::

    .. tab:: GitLab.com

      .. literalinclude:: ../../source/startup-tutorial/intro/.gitlab-ci.com.yml
          :language: yaml
          :linenos:

    .. tab:: ALCF

      .. literalinclude:: ../../source/startup-tutorial/intro/.gitlab-ci-alcf.com.yml
          :language: yaml
          :linenos:

    .. tab:: NERSC

      .. literalinclude:: ../../source/startup-tutorial/intro/.gitlab-ci-nersc.com.yml
          :language: yaml
          :linenos:

Additional Resources
--------------------

We only cover a subset of capabilities throughout this tutorial we think will
most benefit you and your team when getting started. For additional information
and more details please see:

* `GitLab Training <https://about.gitlab.com/training/>`_
* `GitLab Lean <https://about.gitlab.com/learn/>`_

If you are more interested in the scientific software side, of not just
CI/CD but the broader topics of testing we recommend:

* `Better Scientific Software (BSSw) <https://bssw.io/>`_
* The `Spack <https://spack.io/>`_ project has introduced
  `pipelines <https://spack.readthedocs.io/en/latest/pipelines.html?highlight=gitlab#pipelines>`_
  functionality that generates build pipelines for use with
  GitLab CI.
* `Spack GitLab Pipelines at NERSC <https://docs.nersc.gov/applications/e4s/spack_gitlab_pipeline/>`_

Expanded CI Workflow
--------------------

Again for this section of the tutorial we recommend using the
`pipeline editor`_. If you've skipped the introductory section
please copy the appropriate `Completed - Intro CI` for your instance as we will
be starting with that completed job.

In this section we are going to look at features that can
help you and your team with:

* Managing multiple interconnected job across distinct stages
* Establishing meaningful rules to limit CI workflows on HPC resources
* Potential workflows leaving the parallel matrix jobs
* Accounting for some of the complexities of multi-tenant environments

It's important to highlight that we always want to ensure that ECP CI
resources function with established upstream functionality and most CI/CD
recommendations without the need for extensive changes. The exception
to this rule is always when an action would be outside of your users
local permissions.

Lets start by extending the number of stages our pipeline will have:

.. code-block:: yaml

    stages:
      - make
      - run
      - plot

We'll be adding jobs to the newly added ``run`` and ``plot`` stages over
the course of this tutorial. Next we are going to define a few global
`default <https://docs.gitlab.com/ee/ci/yaml/#custom-default-keyword-values>`_
configuration include:

* A basic after_script_ after script to print some arbitrary information
  about our user/environment.
* Setting `interruptible <https://docs.gitlab.com/ee/ci/yaml/#interruptible>`_
  to ``true`` so jobs made redundant by newer pipelines will be cancel to avoid
  wasting potential CI minutes or node hours.
* Establishing a default `timeout <https://docs.gitlab.com/ee/ci/yaml/#timeout>`_
  for our

Keep in mind that any of these, or other potential global values, can
be overridden at the job level.

.. tabs::

    .. tab:: GitLab.com

      .. code-block:: yaml

        default:
          after_script:
            - hostname
            - whoami
          interruptible: true
          timeout: 30m

    .. tab:: ALCF

      .. code-block:: yaml

        default:
          after_script:
            - hostname
            - whoami
          interruptible: true
          timeout: 30m

    .. tab:: NERSC

      .. code-block:: yaml

        default:
          after_script:
            - hostname
            - whoami
          interruptible: true
          timeout: 30m

Lets next write a ``heat-run`` job that will take place in our newly created
``run`` stage_. Since it is in a separate stage and executes after our earlier
``gcc-build`` job it means that the artifact associated with it will be
available and downloaded at the start of ``heat-run``.

.. tabs::

    .. tab:: GitLab.com

      Since we are able to rely on the fact our ``heat`` application will
      already be compiled we can choose a smaller image_, in this case
      simply ``debian:10-slim`` as this should help to speed up the test.

      .. code-block:: yaml

        heat-run:
          stage: run
          image: debian:10-slim
          script:
            - ./heat --help
            - ./heat dx=0.25 maxt=100 ic="rand(125489,100,50)" runame=test

    .. tab:: ALCF

      In ALCF we can't use docker executors, but we can still use artifacts
      among jobs. It worth noting that we want to use tags for every
      stage, otherwise gitlab would place job on a pending state until a runner
      is found that meet the requirements

      .. code-block:: yaml

        heat-run:
          stage: run
          tags: [ecp,shell]
          script:
            - ./heat --help
            - ./heat dx=0.25 maxt=100 ic="rand(125489,100,50)" runame=test

    .. tab:: NERSC

      Each stage must specify a runner for it to execute.

      .. code-block:: yaml

        heat-run:
          stage: run
          tags: [cori]
          script:
            - ./heat --help
            - ./heat dx=0.25 maxt=100 ic="rand(125489,100,50)" runame=test

We are going to run a simple command with our application that will
demonstrate a linear steady state (please see the `Hands-On Lesson`_
for more potential details). This will generate
a series of output files describing the curve in a directly called
``test/``. To allow further potential examination of these results
we should gather them using the artifacts_ mechanism:

.. code-block:: yaml

    heat-run:
      # ...
      artifacts:
        expire_in: 3 days
        paths:
          - test/



Define a hidden job titled ``.manual-rules``. This can later be reused,
allowing the configuration to be reused.

.. code-block:: yaml

    .manual-rule:
      rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
        - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
          when: manual
        - when: never

The rules_ functionality was introduced in GitLab server version 12.3 and has
been expanded through a variety of MRs since then. We highly encourage you
to review to official documentation on this functionality when you use it in
your project. This functionality will allow us to define when jobs might be
expected to run by leveraging CI variables_.

.. note::

    Though for purpose of this tutorial we've chosen to use rules_
    in a very limited fashion, it can be a very important tool for
    limiting CI job especially if you choose to us CI with HPC
    resources.

The next job added will be ``test-plot`` that extends

.. code-block:: yaml

    test-plot:
      stage: plot
      extends:
        - .manual-rule
      needs: [gcc-build heat-run]

Remember rules are evaluated in order until the first match has been
found. In the case of our job ``test-plot`` that inherits the configuration
of ``manual-rules``:

1. Always run if the pipeline has been triggered by a merge request.
2. On the default branch (``main``) the job can be manually started
3. If none of the previous are realized then nothing will be run.

.. tabs::

    .. tab:: GitLab.com

      We've created a basic container that provides access to bother
      `Gnuplot` and `Matplotlib`.

      .. code-block:: yaml

        test-plot:
          # ...
          image: registry.gitlab.com/ecp-ci/ecp-ci.gitlab.io/startup-tutorial/plot:latest
          script:
            - make plot PTOOL=${PTOOL} RUNAME=test

    .. tab:: ALCF

      Again, no docker executors here, but just add the right tag instead.

      .. code-block:: yaml

        test-plot:
          # ...
          tags: [ecp,shell]
          script:
            - make plot PTOOL=${PTOOL} RUNAME=test

    .. tab:: NERSC

      The aforementioned batch runner can be leveraged by using its tag `cori`.
      This runner is available instance-wide on our site-local gitlab instance.

      .. code-block:: yaml

        test-plot:
          # ...
          tags: [cori]
          script:
            - make plot PTOOL=${PTOOL} RUNAME=test

For the final portion of our expanded tutorial we are going to use
the `matrix <https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-jobs>`_
keyword. With this, we are able to multiple jobs in parallel, each
with a different variables value (``PTOOL=???``). Though we are
only highlighting a single-dimensional array, we encourage you to
review the official documentation and further explore this
functionality.

.. code-block:: yaml

    test-plot:
    # ...
    parallel:
      matrix:
        - PTOOL: [gnuplot, matplotlib]

Commit your changes and navigate to your in progress CI pipeline. Keep in mind
the rules_ established earlier, you'll need to manually trigger the jobs on the
``plot`` stage:

.. image:: files/startup/pipeline_blocked_manual.png

Congratulations, once all your tests have has passed you've completed the core
sections of this tutorial. However, if your are still interested in continuing
with this example we've included additional sections below that can be used
to expand the already established pipeline with additional interesting
features that are available across the range of deployments.

Code Coverage (Badge)
~~~~~~~~~~~~~~~~~~~~~

During the introductory section we established a build/test step
that provided a single identifiable value for ``lines`` that had been
accounted for during testing:

.. code-block:: console

    $ gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root ${CI_PROJECT_DIR}
    lines: 45.7% (147 out of 322)
    branches: 30.9% (278 out of 900)

If you have not already done so add the coverage_ flag to identify
this values using regular expressions in our ``gcc-build`` job:

.. code-block:: yaml

      gcc-build:
        coverage: /^\s*lines:\s*\d+.\d+\%/
        # ...

It is possible to leverage this value to clearly demonstrate the total
coverage and status of our pipeline. Navigate to your project's
`Settings` -> `CI/CD` and `Expand` the `General pipelines` section. Here
you will need to identify the *Pipeline status* and *Coverage report*
markdown code snippets as we will be using them to update our
``README.md``.

.. note::

    You may also notice a *Testing coverage parsing* entry. It is also
    possible to update/manage the regular expression for code coverage
    via this GUI section. In our example the entry would be:
    ``lines:\s*\d+.\d+\%``

Next you will need to edit the ``README.md`` file, we recommend using
the `web editor <https://docs.gitlab.com/ee/user/project/repository/web_editor.html>`_.
Simply copy and paste the earlier snippets near the top of your file:

.. image:: files/startup/coverage_edit_readme.png

Once all the changes have been committed and the associated pipeline
completed (please note that because of our earlier defined rules_ you will need
to manually trigger several jobs for the badges to appropriately update).
Once the pipeline has been completed, navigating back to the *Project overview*
you should see the updated badges in your rendered ReadMe section.

Reporting Build Status
~~~~~~~~~~~~~~~~~~~~~~

This final additional effort is built upon the
`Report Build Status to GitLab <https://ecp-ci.gitlab.io/docs/guides/build-status-gitlab.html>`_
guide. Please refer back to that guide for complete details.

The goal is to demonstrate a potential workflow that reports the
status of a CI job to, in this case, a GitLab server. To greatly simplify
the tutorial we've chosen to just report back everything to the same
server/project you are currently working on.

.. image:: ../guides/files/build_status_flow.png
    :scale: 75%

To being with you will need to
`create a personal access token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token>`_
with an *API* scope for your GitLab instance. This will only be required
for this tutorial so either set a short expiration date or revoke
upon competition. Please make note of this newly created token as
we will use it shortly.

This example will make use of a pre-created script (``build-status.py``)
that you can inspect. It in turn relies on a number of environment
variables to establish the target API URL, Project ID, and of course a
personal access token.

From your project space navigate to `Settings` -> `CI/CD` ->
`Variables`. From there you will want to add a new
``BUILDSTATUS_TOKEN`` whose value is the previously created
personal access token. It is highly encouraged that you choose
to mask this value so it does not appear in the CI job log:

.. image:: files/startup/masked_variable.png

.. important::

    A ``Masked`` CI/CD variable will be removed the job log by the
    GitLab-Runner prior to results being posted to the server. This
    means that it can be observed in other files (e.g., scheduler output
    not parsed by the runner, debug logs uploaded to a separate instances,
    or commands run on a multi-tenant system). Please be aware of this
    when deciding how to use any secret in your CI/CD pipelines.

Next we will want to include_
an already created ``build-status.yaml`` file. This feature allows use to
specify an external YAML file, that then will be

.. code-block:: yaml

    include:
     - local: 'build-status.yaml'

Before proceeding it is advisable to quickly review the contents of
the ``build-status.yaml`` file. You'll notice that it add three
new jobs that are members of either the ``.pre`` or ``.post`` stages.
These stages exist by default an will always come at the beginning/end
of a pipeline regardless of other configurations. We've also chosen to
use extends_ with a hidden job, ``.repost-status``, that doesn't and we'll
need to create.

.. code-block:: yaml

    # Contents of the build-status.yaml do not directly
    # insert into your .gitlab-ci.yml file!
    pending:
      extends:
        - .report-status
      stage: .pre

    success:
      extends:
        - .report-status
      stage: .post

    failed:
      extends:
        - .report-status
      stage: .post
      when: on_failure

Circling back to our ``.gitlab-ci.yml`` file we will want to add our
``.report-status`` hidden job there. It is at this point it is important
to highlight that when using include_ and extends_ you are able to
merge elements from different files.

.. tabs::

    .. tab:: GitLab.com

      .. code-block:: yaml

        .report-status:
          variables:
            BUILDSTATUS_JOB: "CI Tutorial"
            BUILDSTATUS_APIURL: "https://gitlab.com/api/v4"
            # Since we are simply using the same project as the target for the POST
            # request we can leverage existing predefined variables.
            BUILDSTATUS_PROJECT: $CI_PROJECT_ID
          image: python:3
          script:
            - pip3 install requests
            - python3 build-status.py
          tags: [gitlab-org]

    .. tab:: ALCF

      To report status against the project, we are going to use an internal URL
      for POST operations. Remember to use the right tags.

      .. code-block:: yaml

        .report-status:
          variables:
            BUILDSTATUS_JOB: "CI Tutorial"
            BUILDSTATUS_APIURL: "https://gitlab-ci.tmi.alcf.anl.gov/api/v4"
            # Since we are simply using the same project as the target for the POST
            # request we can leverage existing predefined variables.
            BUILDSTATUS_PROJECT: $CI_PROJECT_ID
          tags: [ecp,shell]
          script:
            - python3 -m venv .venv
            - source ${CI_PROJECT_DIR}/.venv/bin/activate
            - pip3 install requests
            - python3 build-status.py



    .. tab:: NERSC

      To report status against the project, we are going to use an internal URL
      for POST operations. Remember to use the right tags.

      .. code-block:: yaml

        .report-status:
          variables:
            BUILDSTATUS_JOB: "CI Tutorial"
            BUILDSTATUS_APIURL: "https://software.nersc.gov/api/v4"
            # Since we are simply using the same project as the target for the POST
            # request we can leverage existing predefined variables.
            BUILDSTATUS_PROJECT: $CI_PROJECT_ID
          tags: [cori]
          script:
            - python3 -m venv .venv
            - source ${CI_PROJECT_DIR}/.venv/bin/activate
            - pip3 install requests
            - python3 build-status.py


.. note::

    Though we are able to leverage extends_ across multiple files,
    traditional YAML anchors will only work within a single file.

Now commit the changes to your ``.gitlab-ci.yml`` file and re-run the complete
pipeline. You should now observe several additional jobs/stages added your
pipeline. Include the `CI Tutorial` we created with the ``build-status.py``
script:

.. image:: files/startup/post_external_results.png

.. note::

    In an ideal workflow you would wish to target some other centralized
    instances or test collection mechanism. The specific mechanism of each
    teams strategy for managing remote test results will differ. We hope this
    provides a potential example to work from.

Completed - Expanded CI
~~~~~~~~~~~~~~~~~~~~~~~

Reference of the completed `Expanded CI Workflow`_ ``.gitlab-ci.yml`` file.

.. tabs::

    .. tab:: GitLab.com

      .. literalinclude:: ../../source/startup-tutorial/expanded/.gitlab-ci.com.yml
          :language: yaml
          :linenos:

    .. tab:: ALCF

      .. literalinclude:: ../../source/startup-tutorial/expanded/.gitlab-ci.alcf.yml
          :language: yaml
          :linenos:

    .. tab:: NERSC

      .. literalinclude:: ../../source/startup-tutorial/expanded/.gitlab-ci.nersc.yml
          :language: yaml
          :linenos:



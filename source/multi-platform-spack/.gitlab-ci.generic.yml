stages:
  - prep
  - build
  - test

# Establish variables that will apply to all jobs in the pipeline.
variables:
  # These will be leveraged by a 'spack_deploy.bash' script.
  SPACK_ROOT: "$$HOME/multi-platform-spack"
  SPACK_COMMIT: "0f25462ea63119e3378da11f1e511bedee15b07d"

workflow:
  # Will ensure that the CI pipeline will only run when initiated
  # via the web GUI (CI / CD -> Pipelines -> Run Pipeline).
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always

# Target platform one via runner specific tags.
.platform_one:
  tags: [slurm, ecp-ci]
  # We are leveraging a batch executor and as
  # such we will need to provide the job submission parameters.
  # In this case it is for Slurm.
  variables:
    SCHEDULER_PARAMETERS:  "-N 1 -p ecp-p9-4v100"

# Target platform two via runner specific tags.
.platform_two:
  tags: [slurm, ecp-ci]
  variables:
    SCHEDULER_PARAMETERS:  "-N 1 -p ecp-x86_64"

.source_spack:
  before_script:
    - source ${SPACK_ROOT}/spack/share/spack/setup-env.sh

# Share this common section of code across jobs/machines.
.build_spack_stage:
  extends:
    - .source_spack
  stage: build
  script:
    - spack env activate .
    - spack install lulesh +openmp~mpi os=cnl7 target=haswell
  # Capturing artifacts is optional; however, in our case since
  # we will leverage the 'needs' functionality in GitLab we
  # can rely on that to ensure the correct artifact is downloaded
  # by the dependent.
  artifacts:
    paths:
      - spack.lock
    expire_in: 1d

# Share this common section of code across jobs/machines.
.test_spack_stage: 
  extends:
    - .source_spack
  stage: test
  script:
    - spack env activate .
    - spack load lulesh +openmp~mpi
    - export OMP_NUM_THREADS=2
    - lulesh2.0 -p -i 90    

# No we are going to use the hidden keys defined above by extending them
# into our established jobs. This will end up with a pipeline like:
#
#
#                 -- build_platform_one -- test_platform_one
#                /
# deploy_spack --
#                \
#                 -- build_platform_two -- test_platform_two
#

deploy_spack:
  # Need to deploy, in our case all platforms can access a single
  # shared file system. So we've chosen to simply have a single job.
  extends:
    - .platform_one
  stage: prep
  script:
    - source deploy_spack.bash
    - spack --version
    - spack compiler find

build_platform_one:
  extends: 
    - .build_spack_stage
    - .platform_one

build_platform_two:
  extends:
    - .build_spack_stage
    - .platform_two

test_platform_one:
  extends:
    - .test_spack_stage
    - .platform_one
  # Needs allow us to specify a job from a previous stage, creating
  # a functional directed acyclic graph.
  needs:
    - build_platform_one

test_platform_two:
  extends:
    - .test_spack_stage
    - .platform_two
  needs:
    - build_platform_two
